package info.androidhive.tabsswipe.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParserException;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;

public class ApplicationUtils {

	//private static ArrayList<DataStore> listData;
	private static ArrayList<String> listData;

	/**
	 * Method to get the connection details
	 * 
	 * @return boolean
	 */
	public static boolean isConnectionAvailable(Context context) {

		ConnectivityManager connectivityManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean getXmlFromUrl(String url) {
		boolean isSuccess = false;
		try {
			// defaultHttpClient
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpGet httpGet = new HttpGet(URI.create(url));
			
			HttpResponse httpResponse = httpClient.execute(httpGet);
			HttpEntity httpEntity = httpResponse.getEntity();
			
			XmlParser parser = new XmlParser();

			try {
				InputStream is = httpEntity.getContent();
				
				/*File file = new File(Environment.getExternalStorageDirectory()+"/text.xml");
				if(file.exists()){
					file.delete();
					file.createNewFile();
				}else{
					file.createNewFile();
				}
				
				OutputStream os = new FileOutputStream(file);
				
				int count;
				while((count = is.read()) != -1){
					os.write(count);
				}
				
				os.close();
				is.close();
				
				InputStream is1 = new FileInputStream(file);*/
				
				if (is != null) {
					listData = parser.parse(is);
					Log.d("Test", listData.toString());
					Log.e("Test", ""+listData.size());
					isSuccess = true;
					
					setListData(listData);
				}

			} catch (XmlPullParserException e) {
				isSuccess = false;
				e.printStackTrace();
			} catch (IOException e) {
				isSuccess = false;
				e.printStackTrace();
			}

		} catch (UnsupportedEncodingException e) {
			isSuccess = false;
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			isSuccess = false;
			e.printStackTrace();
		} catch (IOException e) {
			isSuccess = false;
			e.printStackTrace();
		} catch (Exception e) {
			isSuccess = false;
			e.printStackTrace();
		}
		// return XML
		return isSuccess;
	}

	/**
	 * Method to get the listData
	 * 
	 * @return ArrayList<DataStore>
	 */
	public static ArrayList<String> getListData() {
		return ApplicationUtils.listData;
	}

	/**
	 * Method to set the listData
	 * 
	 * @param listData
	 */
	public static void setListData(ArrayList<String> listData) {

		ApplicationUtils.listData = listData;

	}
}
