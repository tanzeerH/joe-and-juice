package info.androidhive.tabsswipe.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public class XmlParser {

	// We don't use namespaces
    private static final String ns = null;
   
    public ArrayList<String> parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, "UTF-8");
            parser.nextTag();
            return readFeed(parser);
        } finally {
            in.close();
        }
    }
    
    private ArrayList<String> readFeed(XmlPullParser parser) throws XmlPullParserException, IOException {
        ArrayList<String> entries = new ArrayList<String>();

        parser.require(XmlPullParser.START_TAG, ns, "data");
        
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            
            if (name.equals("set")) {
                entries.add(readEntry(parser));
            } else {
                skip(parser);
            }
        } 
        return entries;
    }
    
    private String readEntry(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "set");
        String title = null;
        String detail = null;
        String image = null;
        String footer = null;
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
           /* if (name.equals("title")) {
                title = readTitle(parser);
            } 
            else if (name.equals("detail")) {
                detail = readDetail(parser);
            } else 
            */
            if (name.equals("image")) {
                image = readImage(parser);
                
            } 
            /*else if (name.equals("footer")) {
                footer = readFooter(parser);
            }*/ else {
                skip(parser);
            }
        }
        return image;
    }
    
    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "title");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "title");
        return title;
    }
    
    private String readDetail(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "detail");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "detail");
        return title;
    }
    
    private String readImage(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "image");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "image");
        return title;
    }
    
    private String readFooter(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, ns, "footer");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "footer");
        return title;
    }
    
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
    
    private void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
            case XmlPullParser.END_TAG:
                depth--;
                break;
            case XmlPullParser.START_TAG:
                depth++;
                break;
            }
        }
     }
    
}
