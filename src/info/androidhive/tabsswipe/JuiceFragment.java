package info.androidhive.tabsswipe;

import info.androidhive.tabsswipe.adapter.JuiceBarListViewAdaptar;
import info.androidhive.tabsswipe.comparator.JuiceBarSortByDistance;
import info.androidhive.tabsswipe.model.JuiceBar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.nineoldandroids.view.animation.AnimatorProxy;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;

import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import dk.appografen.joeandthejuice.R;

public class JuiceFragment extends Fragment implements OnItemClickListener {

	private ListView listView;
	private JuiceBarListViewAdaptar adapter;
	ArrayList<JuiceBar> listbars = new ArrayList<JuiceBar>();
	String[] itemNames;
	View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// getActivity().getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
		rootView = inflater.inflate(R.layout.juicebarer, container, false);

		listView = (ListView) rootView.findViewById(R.id.listjuicebars);
		getJuiceBars();

		return rootView;
	}

	private void setData() {
		Collections.sort(listbars, new JuiceBarSortByDistance());
		adapter = new JuiceBarListViewAdaptar(getActivity(), R.layout.juicebarer, listbars);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	private void getJuiceBars() {
		itemNames = getResources().getStringArray(R.array.locations_names);
		List<String> names = Arrays.asList(itemNames);
		String[] itemcroods = getResources().getStringArray(R.array.locations_coords);
		List<String> croods = Arrays.asList(itemcroods);
		int i;
		for (i = 0; i < 30; i++) {
			String str[] = names.get(i).split("\n");
			String[] temp = croods.get(i).split(",");
			double lat = Double.valueOf(temp[0]);
			double lon = Double.valueOf(temp[1]);
			// getDistance(lat,lon);
			JuiceBar object = new JuiceBar(str[0], str[1], getDistance(lat, lon) / 1000, lat, lon);
			listbars.add(object);

		}
		setData();

	}

	public double getDistance(double tolat, double tolon) {
		Location locA = new Location("locA");
		locA.setLatitude(Splash.myLatitude);
		locA.setLongitude(Splash.myLongitude);
		Log.e("tag", Splash.myLatitude + "" + Splash.myLongitude);

		Location locB = new Location("LocB");

		locB.setLatitude(tolat);
		locB.setLongitude(tolon);

		double distance = locA.distanceTo(locB);

		return distance;

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		// Log.e("msg", "onitemclick");
		JuiceBarDetailsFragment juicebarFragment = new JuiceBarDetailsFragment();
		Bundle bundle = new Bundle();
		JuiceBar object = listbars.get(position);
		bundle.putString("name", object.getName());
		bundle.putDouble("latitude", object.getLatitude());
		bundle.putDouble("longitude", object.getLongitude());
		bundle.putDouble("distance",object.getDistance());
		juicebarFragment.setArguments(bundle);
		FragmentTransaction rTransaction = getActivity().getSupportFragmentManager().beginTransaction();
		rTransaction.addToBackStack(null);
		rTransaction.hide(this);
		rTransaction.replace(android.R.id.content, juicebarFragment).commit();

	}

}
