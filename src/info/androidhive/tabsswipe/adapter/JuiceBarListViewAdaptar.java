package info.androidhive.tabsswipe.adapter;

import info.androidhive.tabsswipe.model.JuiceBar;
import info.androidhive.tabsswipe.utils.AppUtils;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import dk.appografen.joeandthejuice.R;

public class JuiceBarListViewAdaptar extends ArrayAdapter<JuiceBar> {
	private Context context;

	public JuiceBarListViewAdaptar(Context context, int textViewResourceId,
			List<JuiceBar> items) {
		super(context, textViewResourceId, items);
		this.context = context;
	}

	private class ViewHolder {

		TextView name;
		TextView address;
		TextView distance;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_row_juicebar, null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView
					.findViewById(R.id.textViewname);
			holder.address = (TextView) convertView
					.findViewById(R.id.textViewadddress);
			holder.distance = (TextView) convertView
					.findViewById(R.id.textViewdistance);
			convertView.setTag(holder);

		} else
			holder = (ViewHolder) convertView.getTag();
		JuiceBar juiceBar = getItem(position);
		String tempName = juiceBar.getName();
		holder.name.setText(tempName);
		holder.address.setText(juiceBar.getAddress());
		holder.distance.setText(AppUtils.DecimalCommaFormatter(juiceBar
				.getDistance()) + " km");
		if (tempName.equals("Cph.Airport")) {

		} else if (tempName.equals("Ordrup")) {

		} else if (tempName.equals("Magasin KGS")) {

		} 

			return convertView;
	}

}
