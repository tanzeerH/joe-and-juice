package info.androidhive.tabsswipe.adapter;

import info.androidhive.tabsswipe.model.MenuItem;

import java.util.List;

import com.androidhive.tabsswipe.lazylist.ImageLoader;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import dk.appografen.joeandthejuice.R;

public class MenuBarExpandedListAdapter extends ArrayAdapter<MenuItem> {

	private Context context;
	private ImageLoader loader;

	public MenuBarExpandedListAdapter(Context context, int textViewResourceId,
			List<MenuItem> items) {
		super(context, textViewResourceId, items);
		this.context = context;
		loader=new ImageLoader(context);
	}

	private class ViewHolder {

		TextView name;
		ImageView imageView;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;
		LayoutInflater mInflater = (LayoutInflater) context
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
		if (convertView == null) {
			convertView = mInflater.inflate(R.layout.list_row_menu, null);
			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.list_text);
			holder.imageView = (ImageView) convertView
					.findViewById(R.id.list_image);
			convertView.setTag(holder);

		} else
			holder = (ViewHolder) convertView.getTag();
		MenuItem menuItem = getItem(position);
		holder.name.setText(menuItem.getName());
		
		if(!menuItem.getImageUrl().equals(""))
			loader.getRoundedPicFromURL(menuItem.getImageUrl(),holder.imageView);
			

		return convertView;
	}

}
