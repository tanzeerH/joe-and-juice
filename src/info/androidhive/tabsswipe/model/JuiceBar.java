package info.androidhive.tabsswipe.model;

public class JuiceBar {
	
	private String name;
	private String address;
	private double distance;
	private double latitude;
	private double longitude;
	public JuiceBar(String name,String address,double distance,double latitude, double longitude)
	{
		this.name=name;
		this.address=address;
		this.distance=distance;
		this.latitude=latitude;
		this.longitude=longitude;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public void setAddress(String address)
	{
		this.address=address;
	}
	public  void setDisatnce(double distance)
	{
		this.distance=distance;
	}
	public void setLatitude(double latitude)
	{
		this.latitude=latitude;
	}
	public void setLongitude(double longitude)
	{
		this.longitude=longitude;
	}
	public String getName()
	{
		return this.name;
	}
	public String getAddress()
	{
		return this.address;
	}
	public double getDistance()
	{
		return this.distance;
	}
	public double getLatitude()
	{
		return this.latitude;
	}
	public double getLongitude()
	{
		return this.longitude;
	}
}
