package info.androidhive.tabsswipe.model;

public class MenuItem {
	private String name;
	private String imageURl;
	public MenuItem(String name,String imageUrl)
	{
		this.name=name;
		this.imageURl=imageUrl;
		
	}
	public String getName()
	{
		return this.name;
	}
	public String getImageUrl()
	{
		return this.imageURl;
	}

}
