package info.androidhive.tabsswipe;

import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.TextView;
import dk.appografen.joeandthejuice.R;

public class MenuDetailsFragment extends Fragment {

	TextView textView;
	View rootView;
	private SlidingUpPanelLayout mLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		String name = getArguments().getString("name");

		rootView = inflater.inflate(R.layout.juicebar_details_slide, null, false);
		mLayout = (SlidingUpPanelLayout) rootView.findViewById(R.id.sliding_layout);
		textView = (TextView) rootView.findViewById(R.id.textView1);
		textView.setText("Deatils about " + name + " goes here.");

		return rootView;

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		getView().setFocusableInTouchMode(true);
		getView().requestFocus();
		getView().setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					Log.e("inside","keyevent");
					if (mLayout != null && mLayout.isPanelExpanded() || mLayout.isPanelAnchored()) {
						mLayout.collapsePanel();
						Log.e("inside","expaned");
						return true;
					}

				}
				Log.e("inside","not expaned");
				return false;
			}
		});
		super.onViewCreated(view, savedInstanceState);
	}

}
