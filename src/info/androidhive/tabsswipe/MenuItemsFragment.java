package info.androidhive.tabsswipe;

import info.androidhive.tabsswipe.adapter.MenuBarExpandedListAdapter;
import info.androidhive.tabsswipe.adapter.MenuBarListAdapter;
import info.androidhive.tabsswipe.model.MenuItem;
import info.androidhive.tabsswipe.utils.ApplicationUtils;

import java.util.ArrayList;
import java.util.Arrays;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import dk.appografen.joeandthejuice.R;

public class MenuItemsFragment extends Fragment implements OnItemClickListener {

	private MenuBarExpandedListAdapter adapter;
	private ListView listView;
	String[] items;
	ArrayList<MenuItem> menuItems;
	String[] imageUrls;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.menu_items_expaned, null,
				false);
		listView = (ListView) rootView.findViewById(R.id.mainList2);
		int opNo = getArguments().getInt("id");
		setData(opNo + 1);
		Log.e("msg", "menu items fragment");
		return rootView;

	}

	private void setData(int num) {
		ArrayList<MenuItem> menuItems=new ArrayList<MenuItem>();
		if (num == 1) {
			items = getResources().getStringArray(R.array.item_menu_expaned_1);
			imageUrls=getResources().getStringArray(R.array.image_list_1);
		} else if (num == 2) {
			items = getResources().getStringArray(R.array.item_menu_expaned_2);
			imageUrls=getResources().getStringArray(R.array.image_list_2);
		} else if (num == 3) {
			items = getResources().getStringArray(R.array.item_menu_expaned_3);
			imageUrls=getResources().getStringArray(R.array.image_list_3);
		} else if (num == 4) {
			items = getResources().getStringArray(R.array.item_menu_expaned_4);
			imageUrls=getResources().getStringArray(R.array.image_list_4);
		} else if (num == 5) {
			items = getResources().getStringArray(R.array.item_menu_expaned_5);
			imageUrls=getResources().getStringArray(R.array.image_list_5);
		} else if (num == 6) {
			items = getResources().getStringArray(R.array.item_menu_expaned_6);
			imageUrls=getResources().getStringArray(R.array.image_list_6);
		}
		 
		for(int i=0;i<items.length;i++)
		{
			//Log.e("size",""+ApplicationUtils.getListData().size()+" "+i);
			if(i<imageUrls.length)
				menuItems.add(new MenuItem(items[i],imageUrls[i]));
			else
				menuItems.add(new MenuItem(items[i],""));
			
		}
		Log.e("msg", "" + items.length);
		adapter = new MenuBarExpandedListAdapter(getActivity(), R.layout.list_row_menu,menuItems);

		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		Log.e("msg", "onitemclick");
		MenuDetailsFragment menuFragment = new MenuDetailsFragment();
		Bundle bundle = new Bundle();
		bundle.putString("name", items[position]);
		menuFragment.setArguments(bundle);
		FragmentTransaction rTransaction = getActivity()
				.getSupportFragmentManager().beginTransaction();
		rTransaction.addToBackStack(null);
		rTransaction.hide(this);
		rTransaction.replace(android.R.id.content, menuFragment).commit();

	}

}
