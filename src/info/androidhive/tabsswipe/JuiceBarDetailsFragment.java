package info.androidhive.tabsswipe;

import java.util.UUID;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnKeyListener;
import android.widget.TextView;
import dk.appografen.joeandthejuice.R;

public class JuiceBarDetailsFragment extends Fragment {

	private GoogleMap googleMap;
	View rootView;
	private String juiceBarName = "";
	private double latitude = 0.0;
	private double longitude = 0.0;
	private double distance = 0.0;
	private TextView txtName, txtDisatance;

	private SlidingUpPanelLayout mLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.juicebar_details_with_slide, null, false);
		mLayout = (SlidingUpPanelLayout) rootView.findViewById(R.id.sliding_layout);
		SupportMapFragment mapFragment = (SupportMapFragment) getFragmentManager().findFragmentById(R.id.locationmap);
		txtName = (TextView) rootView.findViewById(R.id.textName);
		txtDisatance = (TextView) rootView.findViewById(R.id.textdistance);
		googleMap = mapFragment.getMap();
		getBundleData();
		setMap();

		return rootView;

	}

	private void getBundleData() {
		juiceBarName = getArguments().getString("name");
		latitude = getArguments().getDouble("latitude");
		longitude = getArguments().getDouble("longitude");
		distance = getArguments().getDouble("distance");
		txtName.setText(juiceBarName);
		txtDisatance.setText(distance + " km");
	}

	private void setMap() {
		CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(latitude, longitude)).zoom(15)
				.build();
		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
		MarkerOptions marker = new MarkerOptions().position(new LatLng(latitude, longitude)).title(juiceBarName);
		googleMap.addMarker(marker);
		googleMap.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng latlng) {

				googleMap.setMyLocationEnabled(true);
			}
		});
		googleMap.getUiSettings().setZoomControlsEnabled(false);

	}
	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		getView().setFocusableInTouchMode(true);
		getView().requestFocus();
		getView().setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_BACK) {
					Log.e("inside","keyevent");
					if (mLayout != null && mLayout.isPanelExpanded() || mLayout.isPanelAnchored()) {
						mLayout.collapsePanel();
						Log.e("inside","expaned");
						return true;
					}

				}
				Log.e("inside","not expaned");
				return false;
			}
		});
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		try {
			SupportMapFragment fragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
					.findFragmentById(R.id.locationmap);
			if (fragment != null)
				getFragmentManager().beginTransaction().remove(fragment).commit();

		} catch (IllegalStateException e) {
			// handle this situation because you are necessary will get
			// an exception here :-(
		}
	}
}
