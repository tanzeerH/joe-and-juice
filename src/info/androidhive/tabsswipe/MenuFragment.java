package info.androidhive.tabsswipe;

import info.androidhive.tabsswipe.adapter.MenuBarListAdapter;
import info.androidhive.tabsswipe.model.MenuItem;
import info.androidhive.tabsswipe.utils.ApplicationUtils;

import java.util.ArrayList;
import java.util.Arrays;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import dk.appografen.joeandthejuice.R;

public class MenuFragment extends Fragment implements OnItemClickListener {

	private MenuBarListAdapter adapter;
	private ListView listView;
	String[] items;
	ArrayList<MenuItem> menuItems;

	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreateView(inflater, container, savedInstanceState);

		View rootView = inflater.inflate(R.layout.menukort, container, false);
		listView = (ListView) rootView.findViewById(R.id.mainListView);
		setData();

		return rootView;

	}

	private void setData() {
		items= getResources().getStringArray(R.array.item_menu);
		String[] imageUrls=getResources().getStringArray(R.array.image_list_main);
		ArrayList<MenuItem> menuItems=new ArrayList<MenuItem>();
		Log.e("items.length",items.length+"");
		for(int i=0;i<items.length;i++)
		{
			menuItems.add(new MenuItem(items[i],imageUrls[i]));
			
		}
		adapter = new MenuBarListAdapter(getActivity(), R.layout.list_row_menu,
				menuItems);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
		MenuItemsFragment menuFragment = new MenuItemsFragment();
		Bundle bundle = new Bundle();
		bundle.putInt("id",position);
		menuFragment.setArguments(bundle);
		FragmentTransaction rTransaction = getActivity()
				.getSupportFragmentManager().beginTransaction();
		rTransaction.addToBackStack(null);
		rTransaction.hide(this);
		rTransaction.replace(android.R.id.content, menuFragment).commit();

	}
}