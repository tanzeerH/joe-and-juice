package info.androidhive.tabsswipe;

import info.androidhive.tabsswipe.utils.ApplicationUtils;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.Settings;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import dk.appografen.joeandthejuice.R;

public class Splash extends FragmentActivity implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener, LocationListener {

	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
	public static double myLatitude = 0.0;
	public static double myLongitude = 0.0;
	private LocationClient locationclient;
	private Location mylocation;
	private LocationRequest locationrequest;
	LocationManager locationManager;
	private boolean isconnected = false;
	private boolean isLocationRetrieved = false;
	private boolean isTimeout = false;
	private boolean isSuccess=false;
	private static int SPLASH_TIME_OUT = 4000;
	
	private static final int BUTTON_POSITIVE = -1;
	private static final int BUTTON_NEGATIVE = -2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {

				LoactionTrack();
			}
		}, SPLASH_TIME_OUT);

		
			
	}
private void LoactionTrack()
{
	LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	if (hasInternet(getApplicationContext())) {
		if (!manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

			/*Toast.makeText(this, "Please turn On Location Services",
					Toast.LENGTH_LONG).show();*/
			showNoLocationDialog();

		} else
			startLoacationTrack();
	}
	else
		Toast.makeText(this, "No internet connection",
				Toast.LENGTH_LONG).show();
}
	public static boolean hasInternet(Context context) {
		ConnectivityManager connectivity = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null) {
				for (int i = 0; i < info.length; i++) {
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public void startLoacationTrack() {
		// location Data
		Log.e("start","start location track");
		locationclient = new LocationClient(this, this, this);
		locationrequest = LocationRequest.create();
		// Use high accuracy
		locationrequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		// Set the update interval to 10 seconds
		locationrequest.setInterval(10000);
		// Set the fastest update interval to 3 second
		locationrequest.setFastestInterval(3000);
		if (servicesConnected())
			locationclient.connect();
		
	}

	@Override
	public void onLocationChanged(Location location) {
		
		Log.e("msg","location changed");
		myLatitude = location.getLatitude();
		myLongitude = location.getLongitude();
		Intent intent = new Intent(getApplicationContext(), MainActivity.class);
		startActivity(intent);
		//new XmlParse().execute();
		

	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
		if (connectionResult.hasResolution()) {
			try {
				// Start an Activity that tries to resolve the error
				connectionResult.startResolutionForResult(this,
						CONNECTION_FAILURE_RESOLUTION_REQUEST);
				/*
				 * Thrown if Google Play services canceled the original
				 * PendingIntent
				 */
			} catch (IntentSender.SendIntentException e) {
				// Log the error
				e.printStackTrace();
			}
		} else {
			/*
			 * If no resolution is available, display a dialog to the user with
			 * the error.
			 */

		}

	}

	@Override
	public void onConnected(Bundle bundle) {
		Log.e("msg","connected");
		mylocation = locationclient.getLastLocation();
		isconnected = true;
		//Toast.makeText(this, "connected", Toast.LENGTH_SHORT).show();
		locationclient.requestLocationUpdates(locationrequest, this);

	}

	@Override
	public void onDisconnected() {
		Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
		case CONNECTION_FAILURE_RESOLUTION_REQUEST:

			break;

		default:
			break;
		}

	}

	@SuppressLint("ValidFragment")
	public class ErrorDialogFragment extends DialogFragment {
		private Dialog mdialog;

		public void setDialog(Dialog dialog) {
			mdialog = dialog;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			return mdialog;
		}
	}

	private boolean servicesConnected() {
		// Check that Google Play services is available
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		// If Google Play services is available
		if (ConnectionResult.SUCCESS == resultCode) {
			// In debug mode, log the status
			Log.d("Location Updates", "Google Play services is available.");
			// Continue
			return true;
			// Google Play services was not available for some reason
		} else {
			// Get the error code
			// int errorCode = ConnectionResult.getErrorCode();
			// Get the error dialog from Google Play services
			Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
					resultCode, this, CONNECTION_FAILURE_RESOLUTION_REQUEST);

			// If Google Play services can provide an error dialog
			if (errorDialog != null) {
				// Create a new DialogFragment for the error dialog
				ErrorDialogFragment errorFragment = new ErrorDialogFragment();
				// Set the dialog in the DialogFragment
				errorFragment.setDialog(errorDialog);
				// Show the error dialog in the DialogFragment
				errorFragment.show(getSupportFragmentManager(),
						"Location Updates");

			}
			return false;
		}
	}
	private void showNoLocationDialog() {

		AlertDialog Alert = new AlertDialog.Builder(Splash.this)
				.create();
		Alert.setTitle("Alert");
		Alert.setMessage("Please Enable Google  Location Services.");

		Alert.setButton(BUTTON_POSITIVE, "Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.cancel();
						finish();

					}
				});

		Alert.setButton(BUTTON_NEGATIVE, "Ok",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
						dialog.cancel();
						Intent myIntent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(myIntent);

					}
				});
		Alert.show();
	}
	@Override
	protected void onStart() {
		super.onStart();

	}

	@Override
	protected void onResume() {

		super.onResume();
	}

	@Override
	protected void onStop() {

		if (isconnected)
			if (locationclient.isConnected())
				locationclient.disconnect();

		super.onStop();
	}
	public class XmlParse extends AsyncTask<Void,Void,Void>
	{

		@Override
		protected Void doInBackground(Void... params) {
			isSuccess=ApplicationUtils.getXmlFromUrl(getString(R.string.contentURL));
			return null;
		}
		@Override
		protected void onPostExecute(Void result) {
			
			Intent intent = new Intent(getApplicationContext(), MainActivity.class);
			startActivity(intent);
			super.onPostExecute(result);
		}
		
	}

}