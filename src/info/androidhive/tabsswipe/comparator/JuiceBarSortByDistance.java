package info.androidhive.tabsswipe.comparator;

import info.androidhive.tabsswipe.model.JuiceBar;

import java.util.Comparator;

public class JuiceBarSortByDistance implements Comparator<JuiceBar> {

	@Override
	public int compare(JuiceBar res1, JuiceBar res2) {
		
		if(res1.getDistance()==res2.getDistance())
			return 0;
		else
			if(res1.getDistance()>res2.getDistance())
				return 1;
			else 
				if(res1.getDistance()<res2.getDistance())
					return -1;
		
		return 0;
		
	}
	
	

}
