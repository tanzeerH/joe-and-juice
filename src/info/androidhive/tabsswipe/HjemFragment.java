package info.androidhive.tabsswipe;

import info.androidhive.tabsswipe.utils.AppUtils;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import dk.appografen.joeandthejuice.R;

public class HjemFragment extends Fragment {

	private Handler handler = new Handler();
	private static int CHANGE_IMAGE_TIME_OUT = 4000;
	private RadioButton rad1, rad2, rad3;
	private ImageView imageView;
	private TextView txt;

	int cnt = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.hjem, container, false);
		rad1 = (RadioButton) rootView.findViewById(R.id.radioButton3);
		rad2 = (RadioButton) rootView.findViewById(R.id.radioButton1);
		rad3 = (RadioButton) rootView.findViewById(R.id.radioButton2);
		imageView = (ImageView) rootView.findViewById(R.id.header1);
		txt = (TextView) rootView.findViewById(R.id.header20);
		handler.post(timedTask);
		setImageAndTexts();
		setRadionButton();
		return rootView;
	}

	private Runnable timedTask = new Runnable() {

		@Override
		public void run() {
			if (AppUtils.IsSlidingImageFragment) {
				cnt++;
				setImageAndTexts();
				setRadionButton();
				// Toast.makeText(getActivity(), "" + cnt,
				// Toast.LENGTH_SHORT).show();
			}
			handler.postDelayed(timedTask, CHANGE_IMAGE_TIME_OUT);
		}
	};

	public void onDestroyView() {
		handler.removeCallbacks(timedTask);
		Log.e("tag", "on destroy view");
		super.onDestroyView();
	};

	private void setRadionButton() {
		rad1.setChecked(false);
		rad2.setChecked(false);
		rad3.setChecked(false);
		if (cnt % 3 == 1) {
			rad1.setChecked(true);
			Log.e("rad", "rad1");
		} else if (cnt % 3 == 2) {
			rad2.setChecked(true);
			Log.e("rad", "rad2");

		}

		else {
			rad3.setChecked(true);
			Log.e("rad", "rad3");
		}
	}

	private void setImageAndTexts() {
		if (cnt % 3 == 1) {

			imageView.setBackgroundResource(R.drawable.front1);
			txt.setText("Text1");
		} else if (cnt % 3 == 2) {
			imageView.setBackgroundResource(R.drawable.front2);
			txt.setText("Text2");

		}

		else {
			imageView.setBackgroundResource(R.drawable.front3);
			txt.setText("Text3");

		}
	}

}
